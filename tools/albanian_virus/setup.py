import sys
from cx_Freeze import setup, Executable

includefiles = []
includes = ['utils','utils.window_force_focuser', 'models']
excludes = []
packages = ['utils', 'tools', 'models']
build_exe_options = {'excludes': excludes, 'packages': packages, 'include_files': includefiles}

base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(
    name='test1',
    version='0.1',
    description='Casdasda',
    author='ZZZ',
    author_email='XXX@YYY.com',
    options={'build_exe': build_exe_options},
    executables=[Executable('albanian_virus.py', base=base)]
)
