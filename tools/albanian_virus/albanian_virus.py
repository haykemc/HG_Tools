import base64
import os
import tkMessageBox
from Tkinter import *
import albanian_flag_base64
from utils.window_force_focuser import WindowMgr

virus_message = "Hi, I am an Albanian virus but because of poor technology in my country unfortunately " \
                "I am not able to harm your computer. Please be so kind to delete one of your important " \
                "files yourself and then forward me to other users. Many thanks for your cooperation! Best " \
                "regards, Albanian virus"

icondata = base64.b64decode(albanian_flag_base64.data)

tempFile = "icon.ico"
iconfile = open(tempFile, "wb")
iconfile.write(icondata)
iconfile.close()
window = Tk()
window.wm_iconbitmap(tempFile)
os.remove(tempFile)


def main():
    window.after(1000, display_popup)
    window.after(5000, focus)
    window.wm_withdraw()
    window.mainloop()


# message at x:200,y:200

def display_popup():
    window.geometry("1x1+200+200")  # remember its .geometry("WidthxHeight(+or-)X(+or-)Y")
    tkMessageBox.showerror(title="Virus Alert !", message=virus_message, parent=window)
    window.after(1000, display_popup)


def focus():
    try:
        wm = WindowMgr()
        wm.find_window_wildcard("Virus Alert !")
        wm.set_foreground()
    except:
        pass
    window.after(5000, focus)


if __name__ == '__main__':
    main()
