import pyHook
import pythoncom
import pyautogui
import win32com.client as comctl

index = 0
forced_message = "mamas 43 empire! "

def OnKeyboardEvent(event):
    global index
    if not event.Injected:
        key = forced_message[index]

        wsh = comctl.Dispatch("WScript.Shell")
        wsh.SendKeys(key)

        index += 1
        if index == len(forced_message):
            index = 0
        return False

hm = pyHook.HookManager()
hm.KeyDown = OnKeyboardEvent
hm.HookKeyboard()
print("Hooked")
pythoncom.PumpMessages()
