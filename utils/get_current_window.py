import win32gui

w = win32gui
def get_focused_window_name():
    return w.GetWindowText(w.GetForegroundWindow())
