import os
import shutil


class ObscurityManager(object):
    def __init__(self, data_storage):
        self.data_storage = data_storage

    # TODO I hate everything about this part of the code, really not sure what im trying to do
    def get_naive_file_location(self, key, source_file, move_file=True, delete_source=False):
        saved_location = self.data_storage.get_value(key, None)
        if saved_location and self.__is_file_already_exists(saved_location, source_file):
            return saved_location, False

        dest_path = self._get_naive_file_path()
        if move_file:
            shutil.copy(source_file, dest_path)

        if delete_source:
            os.remove(source_file)

        self.data_storage.set_value(key, dest_path)
        return dest_path, True

    def set_naive_file_location(self, key, dest_path):
        self.data_storage.set_value(key, dest_path)

    @staticmethod
    def __is_file_already_exists(saved_location, source_file):
        if source_file == saved_location:
            return True  # TODO not sure wtf im doing

        if os.path.exists(saved_location) and os.path.isfile(saved_location):
            if os.path.getsize(saved_location) == os.path.getsize(source_file):  # TODO change to hash check?
                return True

        return False

    # TODO thinkkkkkkkkkkkkkkkkkk
    def _get_naive_file_path(self):
        return r"G:\G_Docs\temp\ffs.whatever"
