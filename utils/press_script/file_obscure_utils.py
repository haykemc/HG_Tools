import os
import uuid

EXTENSIONS = ["exe", "py", "txt"]


def get_path(folder, suffix, extension="exe"):
    files = os.listdir(folder)

    for cur_ext in EXTENSIONS:
        for file_path in files:
            if os.path.exists(file_path) and os.path.isfile(file_path):
                basename, ext = os.path.splitext(file_path)
                if ext == cur_ext:
                    return "{}{}.{}".format(basename, suffix, extension)

    return "{}{}.{}".format(folder, uuid.uuid4(), extension)
