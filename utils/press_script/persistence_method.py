from abc import ABCMeta, abstractmethod


class PersistenceMethod(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def run_method(self, source_file):
        pass
