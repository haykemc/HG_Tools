import sqlite3
from encryption_utils import xor_crypt_string, xor_decrypt_string

encryption_key = "python"  # TODO DECIDE ON A KEY AND NEVER CHANGE IT


class SqliteDataStorage(object):
    def __init__(self, use_encryption=True):
        # TODO set up mutlite files with storage and copy at the end and stuff
        self.use_encryption = use_encryption
        self.db_connection = None
        self._create_tables()

    def _create_tables(self):
        c = self.__get_cursor()
        c.execute('''CREATE TABLE IF NOT EXISTS data (key text NOT NULL UNIQUE, value text)''')
        self.__close_and_commit()

    def get_value(self, key, default_value):
        c = self.__get_cursor()
        c.execute('''SELECT value FROM data WHERE key=?''', [self.__encrypt(key)])
        try:
            value = c.fetchone()[0]
            return self.__decrypt(value)
        except:  # TODO
            return default_value

    def set_value(self, key, value):
        c = self.__get_cursor()
        c.execute('INSERT OR REPLACE INTO data VALUES (? ,?)', [self.__encrypt(key), self.__encrypt(value)])
        self.__close_and_commit()

    def __close_and_commit(self):
        self.db_connection.commit()
        self.db_connection.close()

    def __get_cursor(self):
        self.db_connection = sqlite3.connect(r'G:\G_Docs\temp\example.db')
        return self.db_connection.cursor()

    def __encrypt(self, data):
        if self.use_encryption:
            return xor_crypt_string(data, encryption_key)

        return data

    def __decrypt(self, data):
        if self.use_encryption:
            return xor_decrypt_string(data, encryption_key)

        return data
