﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;


namespace TestService2
{
    public partial class Service1 : ServiceBase
    {
        private Timer _timer;
        public Service1()
        {
            _timer = new Timer(10000);
            _timer.Elapsed += timer_Elapsed;
            InitializeComponent();
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Process p = new Process();
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.StartInfo.FileName = @"C:\Users\user\PycharmProjects\HG_Tools\utils\dist\startup_thing\startup_thing.exe";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            if (System.Environment.OSVersion.Version.Major >= 6)
            {
                p.StartInfo.Verb = "runas";
            }
            p.Start();
        }

        protected override void OnStart(string[] args)
        {
            _timer.Enabled = true;
            _timer.Start();
        }

        protected override void OnStop()
        {
        }
    }
}
